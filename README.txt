CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation

INTRODUCTION
------------
The module helps to keep the experimental code in Devel PHP Execute Code
textarea. It helps when you need to set a list of custom code which will be used
for example for your colleagues from QA team to perform some operations. To test
some features or load some objects you can write a code snippet to get some
information from database directly. To avoid sharing the code snippets by
copy/paste you can do it easily by this module.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/devel_codekeeper

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/devel_codekeeper

REQUIREMENTS
------------
* This module has only one requirement - Devel module (https://www.drupal.org/project/devel).

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
