<?php

/**
 * @file
 * Includes admin pages, forms and callbacks for Devel Codekeeper module.
 */

/**
 * Load snippet callback.
 *
 * @param string $name
 *   The machine name of snippet.
 *
 * @return bool
 *   Return TRUE is snippet is loaded, otherwise - FALSE.
 */
function devel_codekeeper_load($name) {
  $var = variable_get(DEVEL_CODEKEEPER, array());
  return isset($var[$name]) ? $var[$name] : FALSE;
}

/**
 * Delete submit.
 */
function devel_codekeeper_delete_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['redirect'] = 'devel/php/execute/' . $values['machine_name'] . '/delete';
}

/**
 * Validates machine name.
 *
 * @param string $name
 *   Machine name string.
 * @param array $element
 *   Machine name element.
 * @param array $form_state
 *   Form state array.
 *
 * @return bool
 *   If the name is already exist returns FALSE, otherwise - TRUE.
 */
function devel_codekeeper_get_names($name, $element, $form_state) {
  // Do not perform validation when user clicks not on "Save" button.
  if ($form_state['triggering_element']['#name'] != 'devel_codekeeper_save') {
    return TRUE;
  }

  $variable = variable_get(DEVEL_CODEKEEPER, array());
  return isset($variable[$name]);
}

/**
 * Validate callback for devel_execute_form.
 */
function devel_codekeeper_execute_code_validate($form, $form_state) {
  // Initialize the translation context.
  $context = array('context' => DEVEL_CODEKEEPER);
  $values = $form_state['values'];

  foreach (array('name', 'machine_name') as $name) {
    if (empty($values[$name])) {
      form_set_error($name, t('The %name could not be empty', array('%name' => $name), $context));
    }
  }
}

/**
 * Submit callback for devel_execute_form.
 */
function devel_codekeeper_execute_code_save($form, &$form_state) {
  global $user;
  // Initialize the translation context.
  $context = array('context' => DEVEL_CODEKEEPER);

  $data = variable_get(DEVEL_CODEKEEPER, array());
  $values = $form_state['values'];
  $data[$values['machine_name']] = array(
    'name' => $values['name'],
    'code' => $values['code'],
    'user' => $user->name,
    'machine_name' => $values['machine_name'],
  );

  $op = t('saved', array(), $context);
  if (isset($data[$values['machine_name']])) {
    $op = t('updated', array(), $context);
  }

  variable_set(DEVEL_CODEKEEPER, $data);
  drupal_set_message(t('The code snippet %name has been !op successfully.', array(
    '%name' => $values['name'],
    '!op' => $op,
  ), $context));

  $form_state['redirect'] = 'devel/php/execute/' . $values['machine_name'];
}

/**
 * Returns list of saved snippets.
 */
function devel_codekeeper_list() {
  $var = variable_get(DEVEL_CODEKEEPER, array());

  $form['table'] = array(
    '#type'  => 'container',
    '#theme' => 'devel_codekeeper_table',
  );

  foreach ($var as $machine_name => $value) {
    $form['table'][$machine_name]['title'] = array(
      '#markup' => l($value['name'], 'devel/php/execute/' . $machine_name),
    );

    $form['table'][$machine_name]['user'] = array(
      '#markup' => $value['user'],
    );

    $form['table'][$machine_name]['created'] = array(
      '#markup' => date('d-m-Y H:i', time()),
    );

    $form['table'][$machine_name]['link'] = array(
      '#type' => 'link',
      '#title' => t('Edit'),
      '#href' => 'devel/php/execute/' . $machine_name . '/edit',
    );

    $form['table'][$machine_name]['link'] = array(
      '#type' => 'link',
      '#title' => t('Delete'),
      '#href' => 'devel/php/execute/' . $machine_name . '/delete',
    );
  }

  return $form;
}

/**
 * Delete confirmation form.
 */
function devel_codekeeper_delete_form($form, $form_state, $snippet) {
  // Initialize the translation context.
  $context = array('context' => DEVEL_CODEKEEPER);

  $form['snippet'] = array(
    '#type'  => 'value',
    '#value' => $snippet,
  );

  return confirm_form(
    $form,
    t('Are you sure want to delete %name', array('%name' => $snippet['name']), $context),
    'devel/php/execute',
    t('This action cannot be undone.', array(), $context),
    t('Delete', array(), $context),
    t('Cancel', array(), $context)
  );
}

/**
 * Delete submit callback.
 */
function devel_codekeeper_delete_form_submit($form, &$form_state) {
  // Initialize the translation context.
  $context = array('context' => DEVEL_CODEKEEPER);

  $var = variable_get(DEVEL_CODEKEEPER);
  $snippet = $form_state['values']['snippet'];
  unset($var[$snippet['machine_name']]);

  variable_set(DEVEL_CODEKEEPER, $var);
  drupal_set_message(t('The snippet %name has been removed successfully', array('%name' => $snippet['name']), $context));
  $form_state['redirect'] = 'devel/php/codekeeper';
}
