<?php

/**
 * @file
 * Includes theme functions.
 */

/**
 * Returns HTML for a simple table.
 */
function theme_devel_codekeeper_table($vars) {
  // Initialize the translation context.
  $context = array('context' => DEVEL_CODEKEEPER);

  $form = $vars['form'];
  $rows = array();
  foreach (element_children($form) as $id) {
    if (isset($form[$id])) {
      $rows[] = array(
        'data' => array(
          drupal_render($form[$id]['title']),
          drupal_render($form[$id]['user']),
          drupal_render($form[$id]['created']),
          drupal_render($form[$id]['link']),
        ),
        'class' => array(),
      );
    }
  }

  $header = array(
    t('Title', array(), $context),
    t('Author', array(), $context),
    t('Date', array(), $context),
    t('Action', array(), $context),
  );

  $output = theme('table', array(
    'header' => $header,
    'rows'   => $rows,
    'empty'  => t('Table is empty', array(), $context),
  ));

  $output .= drupal_render_children($form);
  return $output;
}
